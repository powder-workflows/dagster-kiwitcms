from setuptools import find_packages, setup

setup(
    name="dagster_kiwitcms",
    version="0.1",
    packages=['dagster_kiwitcms'],
    install_requires=[
        'tcms-api',
    ],
)
